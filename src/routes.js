import { IonicVueRouter } from '@ionic/vue'

const router = new IonicVueRouter({
  routes: [
    { path: '/', redirect: '/app' },
    {
      path: '/app',
      name: 'AppPage',
      component: () =>
        import(/* webpackChunkName: "AppPage" */ '@/pages/AppPage'),
    },
    {
      path: '/sclad',
      name: 'ScladPage',
      component: () =>
        import(/* webpackChunkName: "ScladPage" */ '@/pages/ScladPage'),
    },
    {
      path: '/control',
      name: 'Control',
      component: () =>
        import(/* webpackChunkName: "ControlPage" */ '@/pages/ControlPage'),
    },
  ],
})

export default router
