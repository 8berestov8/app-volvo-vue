import api from "./connect";

export default {
  getOrders() {
    let params = new URLSearchParams();
    return api.get("/findOrders", params);
  },
  clickAssembly(
    status,
    orderId,
    cell,
    dateAssembly,
    dateDone,
    dateDone_ready,
    dateErr,
    dateClose
  ) {
    let params = new URLSearchParams();
    params.append("status", status);
    params.append("cell", cell);
    params.append("orderId", orderId);
    params.append("dateAssembly", dateAssembly);
    params.append("dateDone", dateDone);
    params.append("dateDone_ready", dateDone_ready);
    params.append("dateErr", dateErr);
    params.append("dateClose", dateClose);
    return api.post("/newStatus", params);
  },
};
