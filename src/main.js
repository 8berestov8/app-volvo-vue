import Vue from 'vue'
import App from './App.vue'
import Ionic from '@ionic/vue'
import { IonicVueRouter } from '@ionic/vue'
import '@ionic/core/css/core.css'
import '@ionic/core/css/ionic.bundle.css'
import router from './routes'
import VModal from 'vue-js-modal'
import { store } from './store'

const moment = require('moment')
require('moment/locale/ru')

Vue.config.productionTip = false
Vue.use(Ionic)
Vue.use(IonicVueRouter)
Vue.use(VModal)
Vue.use(require('vue-moment'), {
  moment,
})

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')
