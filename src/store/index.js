import Vue from 'vue'
import Vuex from 'vuex'

import api from '../API/api'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    orders: null,
  },
  getters: {
    ORDERS: (state) => {
      return state.orders
    },
  },
  mutations: {
    SET_ORDERS: (state, payload) => {
      state.orders = payload
    },
  },
  actions: {
    GET_ORDERS: async (context) => {
      try {
        const { data } = await api.getOrders()
        context.commit('SET_ORDERS', data)
      } catch (error) {
        console.error(error)
      }
    },
  },
})
